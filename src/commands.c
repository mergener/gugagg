#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "commands.h" 

Command* commandList[NUMBER_OF_COMMANDS];
static int _commandLastIndex = 0;

//Função para criar um comando.
static Command* NewCmd(char cmd[], char desc[], void (*effect)() ){
    commandList[_commandLastIndex] = (Command*)malloc(sizeof(Command*));
    commandList[_commandLastIndex]->cmd = cmd;
    commandList[_commandLastIndex]->effect = effect;
    _commandLastIndex += 1;
    return commandList[_commandLastIndex-1];
}

//Gerador de Comandos:
void GenerateCommands(){
    NewCmd("ajuda", "Exibe todos os comandos e suas funcoes.", cmd_Ajuda);
}

void CallCommand(char* cmd){
    for (int i = 0; i<=_commandLastIndex; i++){
        if (strcmp(cmd, commandList[i]->cmd) == 0){
            commandList[i]->effect();
            return;
        }
        printf("Comando desconhecido.");
    }
}

//===================================================================
//Efeitos de comandos:
//===================================================================

void cmd_Ajuda(){
    printf("Lista de comandos e suas funcoes: \n\n");
    for (int i = 0; i<=_commandLastIndex; i++){
        puts("'");
        puts(commandList[i]->cmd);
        puts("' - ");
        puts(commandList[i]->desc);
        puts("\n");
    }
}

void cmd_NovoAlbum(){
}