#ifndef album_h
#define album_h

typedef struct Album{
    char *name;
    int *OwnedPlayers[352]; 
    int NumberOfCards;   
} Album;

extern void AddPlayer(Album* album, int player);
extern void RemovePlayer(Album* album, int player);

#endif
